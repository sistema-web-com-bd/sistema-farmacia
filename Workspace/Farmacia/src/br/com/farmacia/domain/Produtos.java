package br.com.farmacia.domain;

public class Produtos {
	
	private Long codigoproduto;
	private String descricaoproduto;
	private Long quantidade;
	private Double preco;
	private Fornecedores fornecedores = new Fornecedores();
	
	public Long getCodigoproduto() {
		return codigoproduto;
	}
	public void setCodigoproduto(Long codigoproduto) {
		this.codigoproduto = codigoproduto;
	}
	public String getDescricaoproduto() {
		return descricaoproduto;
	}
	public void setDescricaoproduto(String descricaoproduto) {
		this.descricaoproduto = descricaoproduto;
	}
	public Long getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(Long quantidade) {
		this.quantidade = quantidade;
	}
	public Double getPreco() {
		return preco;
	}
	public void setPreco(Double preco) {
		this.preco = preco;
	}
	public Fornecedores getFornecedores() {
		return fornecedores;
	}
	public void setFornecedores(Fornecedores fornecedores) {
		this.fornecedores = fornecedores;
	}
	
	

}
